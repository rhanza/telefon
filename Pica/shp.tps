<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.3</string>
        <key>fileName</key>
        <string>F:/Projects/Telefon/Pica/shp.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Telefon/Assets/Images/Main.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Images/Atmosphere.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>205,203,411,405</rect>
                <key>scale9Paddings</key>
                <rect>205,203,411,405</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Background2048.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>512,512,1024,1024</rect>
                <key>scale9Paddings</key>
                <rect>512,512,1024,1024</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/DOT.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>0,0,1,1</rect>
                <key>scale9Paddings</key>
                <rect>0,0,1,1</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Earth.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>182,182,364,364</rect>
                <key>scale9Paddings</key>
                <rect>182,182,364,364</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/ExtraHealth.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>58,58,116,116</rect>
                <key>scale9Paddings</key>
                <rect>58,58,116,116</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Health.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,44,87,87</rect>
                <key>scale9Paddings</key>
                <rect>44,44,87,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Home.png</key>
            <key type="filename">Images/Resurs_10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,35,71,71</rect>
                <key>scale9Paddings</key>
                <rect>35,35,71,71</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Moon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,34,67,67</rect>
                <key>scale9Paddings</key>
                <rect>34,34,67,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Nozama.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,20,48,40</rect>
                <key>scale9Paddings</key>
                <rect>24,20,48,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Restart.png</key>
            <key type="filename">Images/Sound-off.png</key>
            <key type="filename">Images/Sound.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,49,97,97</rect>
                <key>scale9Paddings</key>
                <rect>49,49,97,97</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>184,185,369,369</rect>
                <key>scale9Paddings</key>
                <rect>184,185,369,369</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Telefon.png</key>
            <key type="filename">Images/amazon/0000.png</key>
            <key type="filename">Images/amazon/0001.png</key>
            <key type="filename">Images/amazon/0002.png</key>
            <key type="filename">Images/amazon/0003.png</key>
            <key type="filename">Images/amazon/0004.png</key>
            <key type="filename">Images/amazon/0005.png</key>
            <key type="filename">Images/amazon/0006.png</key>
            <key type="filename">Images/amazon/0007.png</key>
            <key type="filename">Images/amazon/0008.png</key>
            <key type="filename">Images/amazon/0009.png</key>
            <key type="filename">Images/amazon/0010.png</key>
            <key type="filename">Images/amazon/0011.png</key>
            <key type="filename">Images/amazon/0012.png</key>
            <key type="filename">Images/amazon/0013.png</key>
            <key type="filename">Images/amazon/0014.png</key>
            <key type="filename">Images/amazon/0015.png</key>
            <key type="filename">Images/amazon/0016.png</key>
            <key type="filename">Images/amazon/0017.png</key>
            <key type="filename">Images/amazon/0018.png</key>
            <key type="filename">Images/amazon/0019.png</key>
            <key type="filename">Images/amazon/0020.png</key>
            <key type="filename">Images/amazon/0021.png</key>
            <key type="filename">Images/amazon/0022.png</key>
            <key type="filename">Images/amazon/0023.png</key>
            <key type="filename">Images/amazon/0024.png</key>
            <key type="filename">Images/amazon/0025.png</key>
            <key type="filename">Images/amazon/0026.png</key>
            <key type="filename">Images/amazon/0027.png</key>
            <key type="filename">Images/amazon/0028.png</key>
            <key type="filename">Images/amazon/0029.png</key>
            <key type="filename">Images/amazon/0030.png</key>
            <key type="filename">Images/telefon/0000.png</key>
            <key type="filename">Images/telefon/0001.png</key>
            <key type="filename">Images/telefon/0002.png</key>
            <key type="filename">Images/telefon/0003.png</key>
            <key type="filename">Images/telefon/0004.png</key>
            <key type="filename">Images/telefon/0005.png</key>
            <key type="filename">Images/telefon/0006.png</key>
            <key type="filename">Images/telefon/0007.png</key>
            <key type="filename">Images/telefon/0008.png</key>
            <key type="filename">Images/telefon/0009.png</key>
            <key type="filename">Images/telefon/0010.png</key>
            <key type="filename">Images/telefon/0011.png</key>
            <key type="filename">Images/telefon/0012.png</key>
            <key type="filename">Images/telefon/0013.png</key>
            <key type="filename">Images/telefon/0014.png</key>
            <key type="filename">Images/telefon/0015.png</key>
            <key type="filename">Images/telefon/0016.png</key>
            <key type="filename">Images/telefon/0017.png</key>
            <key type="filename">Images/telefon/0018.png</key>
            <key type="filename">Images/telefon/0019.png</key>
            <key type="filename">Images/telefon/0020.png</key>
            <key type="filename">Images/telefon/0021.png</key>
            <key type="filename">Images/telefon/0022.png</key>
            <key type="filename">Images/telefon/0023.png</key>
            <key type="filename">Images/telefon/0024.png</key>
            <key type="filename">Images/telefon/0025.png</key>
            <key type="filename">Images/telefon/0026.png</key>
            <key type="filename">Images/telefon/0027.png</key>
            <key type="filename">Images/telefon/0028.png</key>
            <key type="filename">Images/telefon/0029.png</key>
            <key type="filename">Images/telefon/0030.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,35,51,70</rect>
                <key>scale9Paddings</key>
                <rect>26,35,51,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Tutorial.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>143,117,286,234</rect>
                <key>scale9Paddings</key>
                <rect>143,117,286,234</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Images/Ugle/GoogleAnimate00000.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00001.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00002.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00003.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00004.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00005.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00006.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00007.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00008.png</key>
            <key type="filename">Images/Ugle/GoogleAnimate00009.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,41,82,82</rect>
                <key>scale9Paddings</key>
                <rect>41,41,82,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>Images</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
