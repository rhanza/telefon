﻿using System;
using UnityEngine;

namespace Model
{
    public class EarthModel
    {
        public Settings Params;
        public float LeftButtonPressed;
        public float RightButtonPressed;
        public Vector2 Center;
        public Vector2 RussiaPoint;
        public float Radius;

        public EarthModel(Settings settings)
        {
            Params = settings;
            LeftButtonPressed = 0f;
            RightButtonPressed = 0f;
        }

        [Serializable]
        public class Settings
        {
            public float RotateMirltiplicator = 1f;
            public float MaxRotationSpeed;
            public float MinRotationConstSpeed;
            public float ConstForce;
        }
    }
}