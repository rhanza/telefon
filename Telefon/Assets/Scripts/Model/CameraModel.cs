﻿using System;

namespace Model
{
    public class CameraModel
    {
        public Settings Params;

        public CameraModel(Settings settings)
        {
            Params = settings;
        }
        
        [Serializable]
        public class Settings
        {
            public float Duration;
            public float Amount;
            public float SmoothTimeX;
            public float SmoothTimeY;
            public float Size;
            public float PauseKoef;
            public float DeltaPosition;
        }
    }
}