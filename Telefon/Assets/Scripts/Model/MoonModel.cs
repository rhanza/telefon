﻿using System;

namespace Model
{
    public class MoonModel
    {
        public Settings Params;

        public MoonModel(Settings settings)
        {
            Params = settings;
        }

        [Serializable]
        public class Settings
        {
            public float RotateSpeed;
        }
    }
}