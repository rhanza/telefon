﻿using System;
using UniRx;
using Zenject;

namespace Model
{
    public class SettingsModel
    {
        public Settings Params;

        public SettingsModel(Settings settings)
        {
            Params = settings;
        }

        [Serializable]
        public class Settings
        {
            public int InitSoundsEnabled;
            public float SmoothPauseTime;
            public float DamageTime;
        }
        
        public class SoundEnabledSignal : Signal<SoundEnabledSignal, int>
        {
        }
    }
}