﻿using System;
using UnityEngine;
using View;
using Zenject;

namespace Model
{
    public class MessageModel
    {
        public Settings Params;

        public MessageModel(Settings settings)
        {
            Params = settings;
        }

        [Serializable]
        public class Settings
        {
            public MessageView[] Messages;
            public ExplosionView Explosion;
            public float SpawnTime;
            public float MinSpeed;
            public float MaxSpeed;
            public float MinRadiusPart = 0.5f;
            public float MaxRadiusPart = 1.5f;
            public int Damage = 10;
            public int Bonus = 10;
        }

        public class SpawnVariables
        {
            public Vector2 CenterStartPosition;
            public float Speed;
            public float Direction;
            public float Radius;
        }

        public class MessagePlantedSignal : Signal<MessagePlantedSignal, Vector2, MessageModel.Type>
        {
        }

        public enum Type
        {
            Telefon,
            NotTelefon
        }
    }
}