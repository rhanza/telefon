﻿using System;
using UniRx;

namespace Model
{
    public class PlayerModel
    {
        public Settings Params;
        public IntReactiveProperty CurrentHp;
        public IntReactiveProperty CurrentPoints;
        public BoolReactiveProperty PauseInProgress;

        public PlayerModel(Settings settings)
        {
            Params = settings;
            CurrentHp = new IntReactiveProperty(settings.MaxHp);
            CurrentPoints = new IntReactiveProperty();
            PauseInProgress = new BoolReactiveProperty();
        }

        [Serializable]
        public class Settings
        {
            public int MaxHp;
            public float StopDelta = 0.1f;
            public bool IsGameOver = false;
        }
    }
}