﻿using Model;
using Mvp;
using Presenter;
using UnityEngine;
using Zenject;

namespace View
{
    public class MessageView : View<MessageView, MessagePresenter>
    {
        public MessageModel.Type MessageType;

        public void SetPosition(Vector2 position)
        {
            Transform.position = position;
        }
        
        public void SetLookRotation(Vector3 look)
        {
            var dir = (look - Transform.position);
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            Transform.rotation = Quaternion.Euler(0f, 0f, angle - 90f);
        }

        private void Reinitialize(MessageModel.SpawnVariables variables)
        {
            Presenter.Reinitialize(variables);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Presenter.OnColliderEnter(other);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            Presenter.OnColliderExit(other);
        }

        public class Pool : MonoMemoryPool<MessageModel.SpawnVariables, MessageView>
        {
            protected override void Reinitialize(MessageModel.SpawnVariables variables, MessageView message)
            {
                message.Reinitialize(variables);
            }
        }

        public void OnDespawn()
        {
        }

        public void OnRespawned(Pool factory)
        {
            Presenter.Factory = factory;
        }
       
    }
}