﻿using Mvp; 
using Presenter;
using UnityEngine;
using Zenject;

namespace View
{
    public class ExplosionView : View<ExplosionView, ExplosionPresenter>
    {
        private ParticleSystem _particleSystem;

        protected override void Awake()
        {
            base.Awake();
            _particleSystem = GetComponent<ParticleSystem>();
        }

        public void Boom()
        {
//            Handheld.Vibrate();
            _particleSystem.Play();
        }
        
        public void OnParticleSystemStopped()
        {
            Presenter.OnParticleSystemStopped();
        }
        
        public void SetPosition(Vector2 position)
        {
            Transform.position = position;
        }
        
        public void SetRotation(Quaternion rotation)
        {
            Transform.rotation = rotation;
        }
        
        private void Reinitialize(Vector2 position)
        {
            _particleSystem.Clear();
            Presenter.Reinitialize(position);
        }
        
        public class Pool : MonoMemoryPool<Vector2, ExplosionView>
        {
            protected override void Reinitialize(Vector2 pos, ExplosionView explosion)
            {
                explosion.Reinitialize(pos);
            }
        }
    }
}