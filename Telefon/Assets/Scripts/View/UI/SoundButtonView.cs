﻿using Mvp;
using Presenter.UI;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace View.UI
{
    public class SoundButtonView : View<SoundButtonView, SoundButtonPresenter>
    {
        private Button _button;

        public Sprite EnableSprite;
        public Sprite DisableSprite;

        protected override void FindComponents()
        {
            base.FindComponents();
            _button = GetComponent<Button>();
        }

        protected override void Awake()
        {
            base.Awake();
            _button.OnClickAsObservable().Subscribe(_ => OnClick());
        }

        private void OnClick()
        {
            Presenter.OnClick();
        }

        public void SetSoundButtonState(int isEnabled)
        {
            _button.image.sprite = isEnabled == 1 ? EnableSprite : DisableSprite;
        }
    }
}