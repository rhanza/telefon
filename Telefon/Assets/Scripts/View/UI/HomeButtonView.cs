﻿using Mvp;
using Presenter.UI;
using UniRx;
using UnityEngine.UI;

namespace View.UI
{
    public class HomeButtonView : View<HomeButtonView, HomeButtonPresenter>
    {
        protected override void Awake()
        {
            base.Awake();
            GetComponent<Button>().OnClickAsObservable().Subscribe(_ => OnClick());
        }

        private void OnClick()
        {
            Presenter.OnClick();
        }

        public void SetEnable(bool isEnabled)
        {
            gameObject.SetActive(isEnabled);
        }
    }
}