﻿using Presenter.UI;
using Mvp;
using UnityEngine.UI;


namespace View.UI
{
    public class BonusView : View<BonusView, BonusPresenter>
    {
        private int _initBonus = 0;
        private Text _text;
        
        protected override void Awake()
        {
            base.Awake();
            _text = GetComponent<Text>();
            SetBonus(_initBonus);

        }

        public void SetBonus(int value)
        {
            if (_text == null)
            {
                _initBonus = value;
            }
            else
            {
                _text.text = value.ToString();
            }
        }
    }
}