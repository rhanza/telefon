﻿using MEC;
using Presenter.UI;
using Mvp;
using UnityEngine;
using Utls;

namespace View.UI
{
    public class DamageView : View<DamageView, DamagePresenter>
    {
        private const string Tag = "FLASH";
        private CanvasGroup _canvasGroup;

        protected override void FindComponents()
        {
            base.FindComponents();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Flash(float time)
        {
            Timing.KillCoroutines(Tag);
            Timing.RunCoroutine(Coroutines.Flash(_canvasGroup, time));
        }

        public void SetAlpha(float alpha)
        {
            _canvasGroup.alpha = alpha;
        }
    }
}