﻿using Mvp;
using Presenter.UI;
using UniRx;
using UnityEngine.UI;

namespace View.UI
{
    public class ResumeButtonView : View<ResumeButtonView, ResumePresenter>
    {
        protected override void Awake()
        {
            base.Awake();
            GetComponent<Button>().OnClickAsObservable().Subscribe(_ => OnClick());
        }

        private void OnClick()
        {
            Presenter.OnClick();
        }
    }
}