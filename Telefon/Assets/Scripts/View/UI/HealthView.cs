﻿using Mvp;
using Presenter.UI;
using UnityEngine.UI;

namespace View.UI
{
    public class HealthView : View<HealthView, HealthPresenter>
    {
        private int _initHp = -1;
        private Image _image;

        protected override void Awake()
        {
            base.Awake();
            _image = GetComponent<Image>();
            if (_initHp != -1) SetFillAmount(_initHp);
            
        }

        public void SetHp(int value)
        {
            if (_image == null)
            {
                _initHp = value;
            }
            else
            {
                SetFillAmount(0.01f * value);
            }
        }

        private void SetFillAmount(float value)
        {
            _image.fillAmount = value;
        }
    }
}