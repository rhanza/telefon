﻿using Mvp;
using Presenter.UI;
using UnityEngine.EventSystems;

namespace View.UI
{
    public class ButtonView : View<ButtonView, ButtonPresenter>, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            Presenter.OnPress();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Presenter.OnRelease();
        }
    }
}