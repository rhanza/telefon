﻿using MEC;
using Mvp;
using Presenter.UI;
using UnityEngine;
using Utls;

namespace View.UI
{
    public class MainMenuView : View<MainMenuView, MainMenuPresenter>
    {
        private const string Tag = "FADE";
        private CanvasGroup _canvasGroup;

        protected override void FindComponents()
        {
            base.FindComponents();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Close(float time)
        {
            Timing.KillCoroutines(Tag);
            Timing.RunCoroutine(Coroutines.FadeImage(_canvasGroup, time, true), Tag);
        }

        public void Open(float time, bool isInitial)
        {
            Timing.KillCoroutines(Tag);
            if (isInitial)
            {
                gameObject.SetActive(true);
                _canvasGroup.alpha = 1f;
            }
            else
            {
                Timing.RunCoroutine(Coroutines.FadeImage(_canvasGroup, time, false), Tag);
            }
        }
    }
}