﻿using Mvp;
using Presenter.UI;

namespace View.UI
{
    public class GameOverView : View<GameOverView, GameOverPresenter>
    {
        public void SetEnable(bool isEnabled)
        {
            gameObject.SetActive(isEnabled);
        }
    }
}