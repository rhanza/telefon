﻿using Mvp;
using Presenter;
using UnityEngine;

namespace View
{
    public class MoonView : View<MoonView, MoonPresenter>
    {
        public void Rotate(float delta)
        {
            Transform.Rotate(0f, 0f, delta);
        }

        public void SetRotatePosition(float grads)
        {
            Transform.Rotate(Vector3.forward, grads);
        }
    }
}