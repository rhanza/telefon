﻿using Mvp;
using Presenter;
using UnityEngine;

namespace View
{
    public class SoundsView : View<SoundsView, SoundsPresenter>
    {
        public AudioSource Main;

        protected override void Awake()
        {
            base.Awake();
            Main.loop = true;
        }

        public void ToggleSound(int isEnable)
        {
            Main.mute = isEnable != 1;
            if (isEnable == 1)
            {
                Main.Play();
            }
            else
            {
                Main.Pause();
            }
        }
    }
}