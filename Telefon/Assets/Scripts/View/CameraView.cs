﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using Mvp;
using Presenter;
using UniRx;
using UnityEngine;
using Utls;
using Random = UnityEngine.Random;

namespace View
{
    public class CameraView : View<CameraView, CameraPresenter>
    {
        private Camera _camera;
        
        private const string ShakeTag = "SHAKE";
        private const string SizeTag = "SIZE";

        protected override void FindComponents()
        {
            base.FindComponents();
            _camera = GetComponent<Camera>();
        }

        public void SetPosition(Vector3 position)
        {
            Transform.position = position;
        }

        public void OnStart(float time, float toSize)
        {
            Timing.KillCoroutines(SizeTag);
            Presenter.OnAnimationStarted();
            Timing.RunCoroutine(Coroutines.CameraSizeTo(_camera, time, toSize, true, Presenter), SizeTag);
        }

        public void OnPause(float time, float toSize, bool isInitial)
        {
            Timing.KillCoroutines(SizeTag);
            if (isInitial)
            {
                _camera.orthographicSize = toSize;
            }
            else
            {
                Timing.RunCoroutine(Coroutines.CameraSizeTo(_camera, time, toSize, false, Presenter), SizeTag);
            } 
        }

        public void Shake(float duration, float amount)
        {
            Timing.RunCoroutine(Coroutines.ShakeCoroutine(Transform, duration, amount), ShakeTag);
        }

        
    }
}