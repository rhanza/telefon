﻿using Mvp;
using Presenter;
using UnityEngine;

namespace View
{
    public class EarthView : View<EarthView, EarthPresenter>
    {
        public float AngularVelocity
        {
            get { return _rigidbody.angularVelocity; }
            set { _rigidbody.angularVelocity = value; }
        }

        private Rigidbody2D _rigidbody;

        protected override void Awake()
        {
            base.Awake();
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void AddTorque(float force)
        {
            _rigidbody.AddTorque(force);
        }
    }
}