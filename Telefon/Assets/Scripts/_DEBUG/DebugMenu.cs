﻿using Model;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _DEBUG
{
	public class DebugMenu : MonoBehaviour
	{

		public static DebugMenu Instance;

		[Inject] private EarthModel _earthModel;
		[Inject] private CameraModel _cameraModel;
		[Inject] private MessageModel.Settings _settings;

		private void Awake()
		{
			Instance = this;

			RotMult.text = _earthModel.Params.RotateMirltiplicator.ToString();
			MaxRot.text = _earthModel.Params.MaxRotationSpeed.ToString();
			MinRot.text = _earthModel.Params.MinRotationConstSpeed.ToString();
			SpawnSpeed.text = _settings.SpawnTime.ToString();
			CameraExcentric.text = RussiaPoint.position.y.ToString();
			ShakeDuration.text = _cameraModel.Params.Duration.ToString();
			ShakeAmount.text = _cameraModel.Params.Amount.ToString();
			
			
			RotMult.OnValueChangedAsObservable().Subscribe(v => { _earthModel.Params.RotateMirltiplicator = float.Parse(v); }).AddTo(this);
			MaxRot.OnValueChangedAsObservable().Subscribe(v => { _earthModel.Params.MaxRotationSpeed = float.Parse(v); }).AddTo(this);
			MinRot.OnValueChangedAsObservable().Subscribe(v => { _earthModel.Params.MinRotationConstSpeed = float.Parse(v); }).AddTo(this);
			SpawnSpeed.OnValueChangedAsObservable().Subscribe(v => { _settings.SpawnTime = float.Parse(v); }).AddTo(this);
			CameraExcentric.OnValueChangedAsObservable().Subscribe(v => { RussiaPoint.position  = new Vector3(0f, float.Parse(v), 0f); }).AddTo(this);
			ShakeDuration.OnValueChangedAsObservable().Subscribe(v => { _cameraModel.Params.Duration = float.Parse(v); }).AddTo(this);
			ShakeAmount.OnValueChangedAsObservable().Subscribe(v => { _cameraModel.Params.Amount = float.Parse(v); }).AddTo(this);
		}

		public GameObject[] ToHide;
		public Text HitText;
		public Rigidbody2D Earth;
		public Transform RussiaPoint;

		public InputField RotMult;
		public InputField MaxRot;
		public InputField MinRot;
		public InputField SpawnSpeed;
		public InputField CameraExcentric;
		public InputField ShakeDuration;
		public InputField ShakeAmount;

		public void HideShow()
		{
			foreach (var go in ToHide)
			{
				go.active = !go.active;
			}
		}
	}
}
