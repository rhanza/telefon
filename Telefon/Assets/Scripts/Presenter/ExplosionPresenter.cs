﻿using Mvp;
using UnityEngine;
using View;
using Zenject;

namespace Presenter
{
    public class ExplosionPresenter : Presenter<ExplosionView, ExplosionPresenter>
    {
        private ExplosionView.Pool _factory;

        [Inject]
        public void Construct(ExplosionView.Pool factory)
        {
            _factory = factory;
        }
        
        public void Reinitialize(Vector2 position)
        {
            View.SetPosition(position);
            View.SetRotation(CalcRotation(position));
            View.Boom();
        }

        public void OnParticleSystemStopped()
        {
            _factory.Despawn(View);
        }

        private Quaternion CalcRotation(Vector2 direction)
        {
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            return Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }
}