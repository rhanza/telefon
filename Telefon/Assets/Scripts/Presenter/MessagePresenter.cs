﻿using System;
using Constants;
using LeopotamGroup.Common;
using Manager;
using Model;
using Mvp;
using UniRx;
using UnityEngine;
using View;
using Zenject;
using _DEBUG;
using Variables = Model.MessageModel.SpawnVariables;

namespace Presenter
{
    public class MessagePresenter : Presenter<MessageView, MessagePresenter>
    {
        private EarthModel _earthModel;
        private MessageModel _messageModel;
        private GameManager _gameManager;
        private MessageModel.MessagePlantedSignal _signal;

        private readonly RaycastHit2D[] _hits = new RaycastHit2D[2];

        private MessageModel.Type _messageType;
        private Transform _raycastPoint;
        private IDisposable _job;
        private Variables _variables;

        private bool _isExit;
        
        public MessageView.Pool Factory { get; set; }

        [Inject]
        public void Construct(EarthModel earthModel, 
            MessageModel messageModel,
            PlayerModel playerModel,
            GameManager gameManager,
            MessageModel.MessagePlantedSignal signal)
        {
            _earthModel = earthModel;
            _messageModel = messageModel;
            _gameManager = gameManager;
            _signal = signal;
        }

        public override void Init()
        {
            _raycastPoint = View.Transform.FindRecursiveByTag(Tags.RaycastPointTag);
            _messageType = View.MessageType;
        }

        public void Reinitialize(Variables variables)
        {
            _variables = variables;
            _isExit = false;

            Vector2 startPosition = GetStartPosition(variables.CenterStartPosition);
            
            View.SetPosition(startPosition);
            View.SetLookRotation(GetLookRotation(variables.CenterStartPosition, startPosition, variables.Direction));
            StartMove(variables.Direction);
        }

        public void OnColliderEnter(Collider2D other)
        {
            if (other.CompareTag(Tags.EarthColliderTag) && _isExit)
            {
                HitFromToMessage();
            }
        }

        public void OnColliderExit(Collider2D other)
        {
            if (other.CompareTag(Tags.EarthColliderTag)) _isExit = true;
        }

        private void OnRussiaHit()
        {
            DEBUG_SetHitText("RUSSIA HIT!!!");

            switch (_messageType)
            {
                case MessageModel.Type.Telefon:
                    _gameManager.DoDamage(_messageModel.Params.Damage);
                    break;
                
                case MessageModel.Type.NotTelefon:
                    _gameManager.OnBonus(_messageModel.Params.Bonus);
                    break;
            }
            if (_job != null)
            {
                _job.Dispose();
            }
            Despawn();
        }

        private void OnEarthHit()
        {
            DEBUG_SetHitText("earth hit");
           
            if (_job != null)
            {
                _job.Dispose();
            }
            Despawn();
        }

        private void Despawn()
        {
            View.OnDespawn();
            Factory.Despawn(View);
        }
        
        private Vector2 GetLookRotation(Vector2 centerStartPosition, 
            Vector2 startPosition, float direction)
        {
            return Vector3.Cross(Vector3.forward * direction, startPosition - centerStartPosition);
        }

        private Vector2 GetStartPosition(Vector2 centerPosition)
        {
            float fullLen = (Vector2.Distance(_earthModel.Center, centerPosition));
            Vector2 startPosition =
                centerPosition + (_earthModel.Center - centerPosition) * (_variables.Radius / fullLen);
            return startPosition;
        }

        private void StartMove(float direction)
        {
            _job = Observable
                .EveryUpdate()
                .Subscribe(_ =>
                {
                    View.transform.RotateAround(_variables.CenterStartPosition, Vector3.forward,
                        direction * _variables.Speed * Time.deltaTime);
                })
                .AddTo(View);
        }

        private void HitFromToMessage()
        {
            Vector2 messagePosition = _raycastPoint.position;
            float distance = Vector2.Distance(_earthModel.Center, messagePosition);
            int hits = Physics2D.RaycastNonAlloc(_earthModel.Center, messagePosition, _hits, distance);

            bool isRussia = false;
            
            for (int i = 0; i < hits; i++)
            {
                var hit = _hits[i];
                if (hit.collider.CompareTag(Tags.RussianColliderTag))
                {
                    isRussia = true;
                }
            }
            
            if (isRussia)
            {
                OnRussiaHit();
            }
            else
            {
                OnEarthHit();
            }

            _signal.Fire(messagePosition, _messageType);
        }
        
        //DEBUG
        private void DEBUG_SetHitText(string msg)
        {
            var denugMenu = DebugMenu.Instance;
            if (denugMenu != null)
            {
                denugMenu.HitText.text = msg;
            }

        }
    }
}