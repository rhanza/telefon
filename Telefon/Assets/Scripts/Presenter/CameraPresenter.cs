﻿using Leopotam.Math;
using Manager;
using Model;
using Mvp;
using UniRx;
using UnityEngine;
using View;
using Zenject;

namespace Presenter
{
    public class CameraPresenter : Presenter<CameraView, CameraPresenter>
    {
        private Vector2 _velocity;
        private Vector3 _cachedPosition;

        private CameraModel _model;
        private EarthModel _earthModel;
        private PlayerModel _plaerModel;
        private MessageModel.MessagePlantedSignal _plantedSignalSignal;

        private float _smoothPauseTime;
        private float _originalSize;
        private float _pauseSize;

        [Inject]
        public void Construct(
            SettingsModel settings,
            CameraModel model,
            PlayerModel playerModel,
            EarthModel earthModel,
            MessageModel.MessagePlantedSignal plantedSignalSignal,
            GameManager.StartSignal startSignal,
            GameManager.PauseSignal pauseSignal)
        {
            _smoothPauseTime = settings.Params.SmoothPauseTime;
            _model = model;
            _earthModel = earthModel;
            _plaerModel = playerModel;
            _plantedSignalSignal = plantedSignalSignal;
            startSignal.AsObservable.Subscribe(_ => OnStart());
            pauseSignal.AsObservable.Subscribe(tuple => OnPause(tuple.Item2));
            _originalSize = _model.Params.Size;
            _pauseSize = _model.Params.Size * _model.Params.PauseKoef;
        }

        private void OnPause(bool isInitial)
        {
            View.OnPause(_smoothPauseTime, _pauseSize, isInitial);
        }

        private void OnStart()
        {
            View.OnStart(_smoothPauseTime, _originalSize);
        }

        public override void Init()
        {
            _cachedPosition = View.Transform.position;

            _plantedSignalSignal.AsObservable
                .Subscribe(_ => View.Shake(_model.Params.Duration, _model.Params.Amount))
                .AddTo(View);

            Observable
                .EveryFixedUpdate()
                .Subscribe(_ =>
                {
                    View.SetPosition(CalcPosition());
                }).AddTo(View);
        }

        private Vector3 CalcPosition()
        {
            float xPos = Mathf.SmoothDamp(View.Transform.position.x, _earthModel.RussiaPoint.x, ref _velocity.x,
                _model.Params.SmoothTimeX);
            float yPos = Mathf.SmoothDamp(View.Transform.position.y, _earthModel.RussiaPoint.y, ref _velocity.y,
                _model.Params.SmoothTimeY);
            _cachedPosition.x = xPos;
            _cachedPosition.y = yPos;
            return _cachedPosition;
        }

        public void OnAnimationStarted()
        {
            _plaerModel.PauseInProgress.Value = true;
        }

        public void OnAnimationEnded()
        {
            _plaerModel.PauseInProgress.Value = false;
        }
    }
}