﻿using Model;
using Mvp;
using UniRx;
using View;
using Zenject;

namespace Presenter
{
    public class SoundsPresenter : Presenter<SoundsView, SoundsPresenter>
    {
        private SettingsModel.SoundEnabledSignal _enabledSignal;

        [Inject]
        public void Construct(SettingsModel.SoundEnabledSignal enabledSignal)
        {
            _enabledSignal = enabledSignal;
        }

        public override void Init()
        {
            _enabledSignal.AsObservable.Subscribe(x =>
            {
                View.ToggleSound(x);
            }).AddTo(View);
        }
    }
}