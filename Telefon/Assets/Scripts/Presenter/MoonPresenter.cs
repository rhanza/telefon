﻿using Model;
using Mvp;
using UniRx;
using UnityEngine;
using View;
using Zenject;

namespace Presenter
{
    public class MoonPresenter : Presenter<MoonView, MoonPresenter>
    {
        private MoonModel _moonModel;

        [Inject]
        public void Construct(MoonModel moonModel)
        {
            _moonModel = moonModel;
        }

        public override void Init()
        {
            SetRandomStartPosition();

            Observable
                .EveryUpdate()
                .Subscribe(_ => { Rotate(); })
                .AddTo(View);
        }

        private void SetRandomStartPosition()
        {
            float randomPosition = Random.Range(0f, 360f);
            View.SetRotatePosition(randomPosition);
        }

        private void Rotate()
        {
            float delta = _moonModel.Params.RotateSpeed * Time.deltaTime;
            View.Rotate(delta);
        }
    }
}