﻿                                                                                                                                                                                                                                                                                                                                                                                                                                    using Constants;
using LeopotamGroup.Common;
using Model;
using Mvp;
using UniRx;
using UnityEngine;
using View;
using Zenject;


namespace Presenter
{
    public class EarthPresenter : Presenter<EarthView, EarthPresenter>
    {
        private EarthModel _earthModel;
        private Transform _russiaPoint;

        [Inject]
        public void Construct(EarthModel earthModel)
        {
            _earthModel = earthModel;
        }

        public override void Init()
        {
            _earthModel.Center = View.Transform.position;
            _earthModel.Radius = View.GetComponent<CircleCollider2D>().radius;
            _russiaPoint = View.Transform.FindRecursiveByTag(Tags.RussiaPointTag).transform;

            Observable
                .EveryUpdate()
                .Subscribe(_ => { UpdateRussiaPosition(); })
                .AddTo(View);

            Observable
                .EveryFixedUpdate()
                .Subscribe(_ =>
                {
                    Rotate();
                    RotationForce();
                })
                .AddTo(View);
        }

        private void UpdateRussiaPosition()
        {
            _earthModel.RussiaPoint = _russiaPoint.position;
        }

        private void RotationForce()
        {
            float direction =_earthModel.RightButtonPressed -  _earthModel.LeftButtonPressed;
            if (direction.Equals(0f)) return;
            View.AddTorque(_earthModel.Params.RotateMirltiplicator * direction);
        }

        private void Rotate()
        {
            ClampAngularVelocity();
        }

        private void ClampAngularVelocity()
        {
            var max = _earthModel.Params.MaxRotationSpeed;
            var min = _earthModel.Params.MinRotationConstSpeed;
            if (View.AngularVelocity < -max)
            {
                View.AngularVelocity = -max;
            }

            if (View.AngularVelocity > max)
            {
                View.AngularVelocity = max;
            }

            if (View.AngularVelocity < min && View.AngularVelocity > -min)
            {
                View.AddTorque(_earthModel.Params.ConstForce);
            }
        }
    }
}