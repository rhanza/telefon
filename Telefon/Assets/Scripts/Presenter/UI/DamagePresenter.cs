﻿using Manager;
using Model;
using Mvp;
using UniRx;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class DamagePresenter : Presenter<DamageView, DamagePresenter>
    {
        [Inject]
        public void Construct(GameManager.DamageSignal damageSignal, GameManager.GameOverSignal gameOverSignal, SettingsModel settings)
        {
            float damageTime = settings.Params.DamageTime;
            damageSignal.AsObservable.Subscribe(_ => View.Flash(damageTime));
            gameOverSignal.AsObservable.Subscribe(_ => View.SetAlpha(0f));
        }
    }
}