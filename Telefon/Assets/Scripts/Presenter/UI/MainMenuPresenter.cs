﻿using Manager;
using Model;
using Mvp;
using View.UI;
using Zenject;
using UniRx;

namespace Presenter.UI
{
    public class MainMenuPresenter : Presenter<MainMenuView, MainMenuPresenter>
    {
        [Inject]
        public void Construct(GameManager.PauseSignal pauseSignal, GameManager.StartSignal startSignal, SettingsModel settings)
        {
            float smoothPauseTime = settings.Params.SmoothPauseTime;
            startSignal.AsObservable.Subscribe(_ => View.Close(smoothPauseTime));
            pauseSignal.AsObservable.Subscribe(tuple => View.Open(smoothPauseTime, tuple.Item2));
        }
    }
}