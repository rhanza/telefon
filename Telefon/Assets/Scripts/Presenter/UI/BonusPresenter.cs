﻿using Model;
using Mvp;
using UniRx;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class BonusPresenter : Presenter<BonusView, BonusPresenter> 
    {
        private PlayerModel _player;

        [Inject]
        public void Construct(PlayerModel player)
        {
            _player = player;
        }
        
        public override void Init()
        {
            _player.CurrentPoints.Subscribe(pts =>
            {
                View.SetBonus(pts);
            }).AddTo(View);
        }
    }
}