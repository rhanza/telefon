﻿using Model;
using Mvp;
using UnityEngine;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class SoundButtonPresenter : Presenter<SoundButtonView, SoundButtonPresenter>
    {
        private const string SoundKey = "sound_enabled";
        private SettingsModel _settings;
        private SettingsModel.SoundEnabledSignal _signal;

        [Inject]
        public void Construct(SettingsModel settings, SettingsModel.SoundEnabledSignal signal)
        {
            _settings = settings;
            _signal = signal;
        } 
        
        public override void Init()
        {
            int soundEnabled;
            if (PlayerPrefs.HasKey(SoundKey))
            {
                soundEnabled = PlayerPrefs.GetInt(SoundKey);
            }
            else
            {
                soundEnabled = _settings.Params.InitSoundsEnabled;
                PlayerPrefs.SetInt(SoundKey, soundEnabled);
            }
            SetSoundEnabled(soundEnabled);
        }
        
        public void OnClick()
        {
            int soundEnabled = PlayerPrefs.GetInt(SoundKey);
            int newSoundState = soundEnabled == 1 ? 0 : 1;
            PlayerPrefs.SetInt(SoundKey, newSoundState);
            SetSoundEnabled(newSoundState);
        }

        private void SetSoundEnabled(int isEnabled)
        {
            View.SetSoundButtonState(isEnabled);
            _signal.Fire(isEnabled);
        }
    }
}