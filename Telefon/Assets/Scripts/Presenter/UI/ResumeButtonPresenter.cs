﻿using Manager;
using Mvp;
using UniRx;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class ResumePresenter : Presenter<ResumeButtonView, ResumePresenter>
    {
        private GameManager.StartSignal _startSignal;

        [Inject]
        public void Construct(
            GameManager.StartSignal startSignal,
            GameManager.PauseSignal pauseSignal)
        {
            _startSignal = startSignal;
        }
        
        public void OnClick()
        {
            _startSignal.Fire();
        }
    }
}