﻿using Manager;
using Mvp;
using UniRx;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class HomeButtonPresenter : Presenter<HomeButtonView, HomeButtonPresenter>
    {
        private GameManager _gameManager;

        [Inject]
        public void Construct(GameManager gameManager, GameManager.PauseSignal pauseSignal)
        {
            _gameManager = gameManager;
            pauseSignal.AsObservable.Subscribe(tuple =>
            {
                if (tuple.Item1)
                {
                    View.SetEnable(false);
                }
                else
                {
                    View.SetEnable(true);
                }
            });
        }
        
        public void OnClick()
        {
            _gameManager.OnHome();
        }
    }
}