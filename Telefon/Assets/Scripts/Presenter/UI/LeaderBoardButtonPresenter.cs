﻿using Manager;
using Mvp;
using UniRx;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class LeaderBoardButtonPresenter : Presenter<LeaderBoardButtonView, LeaderBoardButtonPresenter>
    {
        private GameManager _gameManager;

        [Inject]
        public void Construct(GameManager gameManager, GameManager.PauseSignal pauseSignal)
        {
            _gameManager = gameManager;
            pauseSignal.AsObservable.Subscribe((tuple) =>
            {
                if (tuple.Item1)
                {
                    View.SetEnable(true);
                }
                else
                {
                    View.SetEnable(false);
                }
            });
        }
        
        public void OnClick()
        {
            _gameManager.OnLeaderBoard();
        }
    }
}