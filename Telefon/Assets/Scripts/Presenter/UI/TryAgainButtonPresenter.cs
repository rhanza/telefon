﻿using Manager;
using Mvp;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class TryAgainButtonPresenter : Presenter<TryAgainButtonView, TryAgainButtonPresenter>
    {
        private GameManager _gameManager;

        [Inject]
        public void Construct(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void OnClick()
        {
            _gameManager.OnTryAgain();
        }
    }
}