﻿using Manager;
using Mvp;
using UniRx;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class GameOverPresenter : Presenter<GameOverView, GameOverPresenter>
    {
        [Inject]
        public void Construct(GameManager.GameOverSignal gameOverSignal)
        {
            gameOverSignal.AsObservable.Subscribe(_ => View.SetEnable(true));
        }
    }
}