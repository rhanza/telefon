﻿using Manager;
using Model;
using Mvp;
using UniRx;
using UnityEngine;
using View;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class ButtonPresenter : Presenter<ButtonView, ButtonPresenter>
    {
        private GameManager.StartSignal _startSignal;
        private GameManager.PauseSignal _pauseSignal;
        private EarthModel _earthModel;
        private PlayerModel _playerModel;
        private float _stopDelta;
        private float _lastX;

        private RectTransform _rectTransform;
        private Camera _camera;
        private Vector2 _localpoint;

        private bool _isPressed = false;

        [Inject]
        public void Construct(GameManager.StartSignal startSignal,
            GameManager.PauseSignal pauseSignal,
            EarthModel earthModel,
            PlayerModel playerModel)
        {
            _startSignal = startSignal;
            _pauseSignal = pauseSignal;
            _earthModel = earthModel;
            _playerModel = playerModel;

            _stopDelta = _playerModel.Params.StopDelta;
        }

        public override void Init()
        {
            _rectTransform = View.GetComponent<RectTransform>();
            _camera = View.GetComponentInParent<Canvas>().worldCamera;
            Observable.EveryUpdate().SkipWhile(_ => !_isPressed).Subscribe(_ =>
            {
                if (_playerModel.Params.IsGameOver)
                {
                    OnRelease();
                    return;
                }

                if (_playerModel.PauseInProgress.Value)
                {
                    return;
                }
                
                float x = GetNormalizedX();

                if (Mathf.Abs(_lastX - x) < _playerModel.Params.StopDelta)
                {
                    return;
                }
                
                if (x < _lastX)
                {
                    _earthModel.LeftButtonPressed = 1;
                    _earthModel.RightButtonPressed = 0;
                }
                else if (x > _lastX)
                {
                    _earthModel.LeftButtonPressed = 0;
                    _earthModel.RightButtonPressed = 1;
                }

                _lastX = x;
            }).AddTo(View);
        }

        public void OnPress()
        {
            _lastX = GetNormalizedX();
            if (!_playerModel.Params.IsGameOver)
            {
                _isPressed = true;
                _startSignal.Fire();
            }
        }

        public void OnRelease()
        {
            _isPressed = false;
            _pauseSignal.Fire(false, false);
        }

        private float GetNormalizedX()
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, Input.mousePosition, _camera,
                out _localpoint);
            Vector2 normalizedPoint = Rect.PointToNormalized(_rectTransform.rect, _localpoint);
            return normalizedPoint.x;
        }
    }
}