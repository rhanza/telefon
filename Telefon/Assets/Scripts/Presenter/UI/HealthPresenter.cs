﻿using Model;
using Mvp;
using UniRx;
using View.UI;
using Zenject;

namespace Presenter.UI
{
    public class HealthPresenter : Presenter<HealthView, HealthPresenter>
    {
        private PlayerModel _player;

        [Inject]
        public void Construct(PlayerModel player)
        {
            _player = player;
        }
        
        public override void Init()
        {
            _player.CurrentHp.Subscribe(hp =>
            {
                View.SetHp(hp);
            }).AddTo(View);
        }

    }
}