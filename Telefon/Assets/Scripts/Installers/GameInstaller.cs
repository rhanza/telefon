﻿using Manager;
using Model;
using Presenter;
using Presenter.UI;
using View;
using Zenject;

namespace Installers
{
    public class GameInstaller : MonoInstaller
    {
        [Inject] private MessageModel.Settings _spawnSettings;

        public override void InstallBindings()
        {
            InstallGameSettings();
            InstallPresenters();
            InstallManagers();
            InstallCamera();
            InstallSpawn();
            InstallPlayer();
            InstallUi();
            InstallEarth();
            InstallMoon();
            InstallMessage();
        }

        private void InstallPresenters()
        {
            Container.Bind<CameraPresenter>().AsTransient();
            Container.Bind<EarthPresenter>().AsTransient();
            Container.Bind<ExplosionPresenter>().AsTransient();
            Container.Bind<MessagePresenter>().AsTransient();
            Container.Bind<MoonPresenter>().AsTransient();
            Container.Bind<ButtonPresenter>().AsTransient();
            Container.Bind<HealthPresenter>().AsTransient();
            Container.Bind<BonusPresenter>().AsTransient();
            Container.Bind<MainMenuPresenter>().AsTransient();
            Container.Bind<ResumePresenter>().AsTransient();
            Container.Bind<SoundButtonPresenter>().AsTransient();
            Container.Bind<HomeButtonPresenter>().AsTransient();
            Container.Bind<LeaderBoardButtonPresenter>().AsTransient();
            Container.Bind<GameOverPresenter>().AsTransient();
            Container.Bind<TryAgainButtonPresenter>().AsTransient();
            Container.Bind<SoundsPresenter>().AsTransient();
            Container.Bind<DamagePresenter>().AsTransient();
        }

        private void InstallManagers()
        {
            Container.Bind<ITickable>().To<SpawnManager>().AsSingle();
            Container.Bind<GameManager>().AsSingle();
            Container.Bind<IInitializable>().To<GameManager>().AsSingle();
        }

        private void InstallSpawn()
        {
            for (int i = 0; i < _spawnSettings.Messages.Length; i++)
            {
                Container.BindMemoryPool<MessageView, MessageView.Pool>()
                    .WithInitialSize(3).WithId(i)
                    .FromComponentInNewPrefab(_spawnSettings.Messages[i])
                    .UnderTransformGroup("Messages");
            }

            Container.BindMemoryPool<ExplosionView, ExplosionView.Pool>()
                .WithInitialSize(3)
                .FromComponentInNewPrefab(_spawnSettings.Explosion)
                .UnderTransformGroup("Explosions");
        }

        private void InstallCamera()
        {
            Container.Bind<CameraModel>().AsSingle();
        }

        private void InstallEarth()
        {
            Container.Bind<EarthModel>().AsSingle();
        }

        private void InstallMessage()
        {
            Container.Bind<MessageModel>().AsTransient();
            Container.DeclareSignal<MessageModel.MessagePlantedSignal>();
        }

        private void InstallMoon()
        {
            Container.Bind<MoonModel>().AsSingle();
        }

        private void InstallPlayer()
        {
            Container.Bind<PlayerModel>().AsSingle();
        }

        private void InstallUi()
        {
            Container.DeclareSignal<GameManager.StartSignal>();
            Container.DeclareSignal<GameManager.PauseSignal>();
            Container.DeclareSignal<GameManager.GameOverSignal>();
            Container.DeclareSignal<GameManager.DamageSignal>();
        }

        private void InstallGameSettings()
        {
            Container.Bind<SettingsModel>().AsSingle();
            Container.DeclareSignal<SettingsModel.SoundEnabledSignal>();
        }
    }
}