using Manager;
using Model;
using UnityEngine;
using Zenject;

namespace Installers
{
    [CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "Installers/GameSettingsInstaller")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        public SettingsModel.Settings Settings;
        public EarthModel.Settings Earth;
        public MoonModel.Settings Moon;
        public MessageModel.Settings Message;
        public CameraModel.Settings Camera;
        public PlayerModel.Settings Player;
        
        public override void InstallBindings()
        {
            Container.BindInstance(Settings);
            Container.BindInstance(Earth);
            Container.BindInstance(Moon);
            Container.BindInstance(Message);
            Container.BindInstance(Camera);
            Container.BindInstance(Player);
        }
    }
}