﻿using System.Collections.Generic;
using MEC;
using Model;
using Presenter;
using UnityEngine;

namespace Utls
{
    public class Coroutines
    {
        public static IEnumerator<float> SmoothStartPause(float time, bool isStart)
        {
            float speed = 1f / time;

            if (isStart)
            {
                for (float i = Time.timeScale; i <= 1f; i += Time.unscaledDeltaTime * speed)
                {
                    Time.timeScale = i;
                    yield return Timing.WaitForOneFrame;
                }

                Time.timeScale = 1f;
            }
            else
            {
                for (float i = Time.timeScale; i >= 0f; i -= Time.unscaledDeltaTime * speed)
                {
                    Time.timeScale = i;
                    yield return Timing.WaitForOneFrame;
                }

                Time.timeScale = 0;
            }
        }

        public static IEnumerator<float> FadeImage(CanvasGroup canvasGroup, float time, bool fadeAway)
        {
            float speed = 1f / time;

            if (fadeAway)
            {
                for (float i = canvasGroup.alpha; i >= 0; i -= Time.unscaledDeltaTime * speed)
                {
                    canvasGroup.alpha = i;
                    yield return Timing.WaitForOneFrame;
                }

                canvasGroup.gameObject.SetActive(false);
            }
            else
            {
                canvasGroup.gameObject.SetActive(true);
                for (float i = canvasGroup.alpha; i <= 1; i += Time.unscaledDeltaTime * speed)
                {
                    canvasGroup.alpha = i;
                    yield return Timing.WaitForOneFrame;
                }
            }
        }

        public static IEnumerator<float> Flash(CanvasGroup canvasGroup, float time)
        {
            float speed = 1f / time / 2f;

            for (float i = canvasGroup.alpha; i <= 1; i += Time.unscaledDeltaTime * speed)
            {
                canvasGroup.alpha = i;
                yield return Timing.WaitForOneFrame;
            }

            for (float i = canvasGroup.alpha; i >= 0; i -= Time.unscaledDeltaTime * speed)
            {
                canvasGroup.alpha = i;
                yield return Timing.WaitForOneFrame;
            }

            canvasGroup.alpha = 0f;
        }

        public static IEnumerator<float> CameraSizeTo(Camera camera, float time, float to, bool isStart,
            CameraPresenter presenter)
        {
            float speed = 1f / time;
            if (isStart)
            {
                for (float i = camera.orthographicSize; i <= to; i += Time.unscaledDeltaTime * speed)
                {
                    camera.orthographicSize = i;
                    yield return Timing.WaitForOneFrame;
                }

                if (presenter != null)
                {
                    presenter.OnAnimationEnded();
                }
            }

            else
            {
                for (float i = camera.orthographicSize; i >= to; i -= Time.unscaledDeltaTime * speed)
                {
                    camera.orthographicSize = i;
                    yield return Timing.WaitForOneFrame;
                }
            }
        }

        public static IEnumerator<float> ShakeCoroutine(Transform transform, float duration, float amount)
        {
            float elapsedTime = 0f;
            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                if (Time.timeScale > 0f)
                {
                    if (transform != null)
                        transform.localPosition += Random.insideUnitSphere * amount;
                }

                yield return Timing.WaitForOneFrame;
            }
        }
    }
}