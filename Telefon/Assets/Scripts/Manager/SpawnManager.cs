﻿using System.Collections.Generic;
using Model;
using UniRx;
using UnityEngine;
using View;
using Zenject;
using Random = UnityEngine.Random;

namespace Manager
{
    public class SpawnManager : ITickable
    {
        private readonly EarthModel _earthModel;
        private readonly List<MessageView.Pool> _messageFactories;
        private readonly ExplosionView.Pool _explosionFactory;

        private float _lastSpawnTime;

        public float EllapsedTime;

        public MessageModel.Settings _settings;

        public SpawnManager(DiContainer container,
            MessageModel.Settings settings,
            EarthModel earthModel,
            ExplosionView.Pool explosionFactory,
            GameManager gameManager,
            MessageModel.MessagePlantedSignal signal)
        {
            _settings = settings;
            _earthModel = earthModel;
            _explosionFactory = explosionFactory;
            int count = settings.Messages.Length;
            _messageFactories = new List<MessageView.Pool>(count);
            for (int i = 0; i < count; i++)
            {
                _messageFactories.Add(container.ResolveId<MessageView.Pool>(i));
            }

            signal.AsObservable.Subscribe(x => { SpawnExplosion(x.Item1, x.Item2); });
        }

        private void SpawnMessage()
        {
            MessageModel.SpawnVariables vars =
                new MessageModel.SpawnVariables
                {
                    CenterStartPosition = ChooseRandomPosition(),
                    Speed = Random.Range(_settings.MinSpeed, _settings.MaxSpeed),
                    Direction = Random.value > 0.5f ? -1f : 1f
                };

            float minRadius = _earthModel.Radius * _settings.MinRadiusPart;
            float maxRadius = _earthModel.Radius * _settings.MaxRadiusPart;
            vars.Radius = Random.Range(minRadius, maxRadius);

            int randomIndex = Random.Range(0, _settings.Messages.Length);
            var randomFactory = _messageFactories[randomIndex];
            var spawnedMessage = randomFactory.Spawn(vars);
            spawnedMessage.OnRespawned(randomFactory);

            _lastSpawnTime = EllapsedTime;
        }

        private Vector2 ChooseRandomPosition()
        {
            return RandomCircle(_earthModel.Center, _earthModel.Radius);
        }

        private void SpawnExplosion(Vector2 position, MessageModel.Type type)
        {
            _explosionFactory.Spawn(position);
        }

        private Vector2 RandomCircle(Vector3 center, float radius)
        {
            float ang = Random.value * 360;
            Vector2 pos;
            pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
            pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
            return pos;
        }

        public void Tick()
        {
            EllapsedTime += Time.deltaTime;
            if (EllapsedTime - _lastSpawnTime > _settings.SpawnTime)
            {
                SpawnMessage();
            }
        }
    }
}