﻿using MEC;
using Model;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utls;
using Zenject;

namespace Manager
{
    public class GameManager : IInitializable
    {
        private const string Tag = "PAUSESTART";
        
        private bool _isFirstStarted;
        private readonly SettingsModel _settings;
        private readonly PlayerModel _playerModel;
        private readonly PauseSignal _pauseSignal;
        private readonly DamageSignal _damageSignal;
        private readonly GameOverSignal _gameOverSignal;


        public GameManager(SettingsModel settings, PlayerModel playerModel, StartSignal startSignal,
            PauseSignal pauseSignal, DamageSignal damageSignal, GameOverSignal gameOverSignal)
        {
            _settings = settings;
            _playerModel = playerModel;
            _pauseSignal = pauseSignal;
            _damageSignal = damageSignal;
            _gameOverSignal = gameOverSignal;
            startSignal.AsObservable.Subscribe(_ => OnStart());
            _pauseSignal.AsObservable.Subscribe(tuple => OnPause(tuple.Item2));
            _gameOverSignal.AsObservable.Subscribe(_ => GameOver());
        }

        public void DoDamage(int value)
        {
            int currentValue = _playerModel.CurrentHp.Value;
            int calculatedValue = currentValue - value;
            if (calculatedValue <= 0)
            {
                calculatedValue = 0;
                _gameOverSignal.Fire();
                _playerModel.Params.IsGameOver = true;
            }

            _playerModel.CurrentHp.Value = calculatedValue;
            _damageSignal.Fire();
        }

        public void OnBonus(int value)
        {
            _playerModel.CurrentPoints.Value += value;
        }

        public void OnHome()
        {
            RestartGame();
        }

        public void OnTryAgain()
        {
            RestartGame();
        }

        public void OnLeaderBoard()
        {
            Debug.Log("Open Leader Board");
        }

        private void OnPause(bool isInstant)
        {
            Timing.KillCoroutines(Tag);
            if (isInstant)
            {
                Time.timeScale = 0f;
            }
            else
            {
                Timing.RunCoroutine(Coroutines.SmoothStartPause(_settings.Params.SmoothPauseTime, false), Tag);
            }
        }

        private void OnStart()
        {
            Timing.KillCoroutines(Tag);
            Timing.RunCoroutine(Coroutines.SmoothStartPause(_settings.Params.SmoothPauseTime, true), Tag);

        }

        private void RestartGame()
        {
            _playerModel.Params.IsGameOver = false;
            int scene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
        }

        private void GameOver()
        {
            Time.timeScale = 0f;
        }

        public class StartSignal : Signal<StartSignal>
        {
        }

        public class PauseSignal : Signal<PauseSignal, bool, bool>
        {
        }

        public class GameOverSignal : Signal<GameOverSignal>
        {
        }
        
        public class DamageSignal: Signal<DamageSignal>
        {
        }


        public void Initialize()
        {
            _pauseSignal.Fire(true, true);
        }
    }
}