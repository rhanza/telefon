﻿namespace Constants
{
    public static class Tags
    {
        public const string EarthColliderTag = "EarthCollider";
        public const string RussianColliderTag = "RussiaCollider";
        public const string RaycastPointTag = "RayscastPoint";
        public const string RussiaPointTag = "RussiaPoint";
    }
}